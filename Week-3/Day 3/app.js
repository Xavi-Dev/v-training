// different ways to creat objects..

//object literals
let car=  {
    make: 'Tesla',
    model: 'S',
    year: 2012
}


//object.create
let carCreate = Object.create(Object.prototype, {
    make: {
        value: ' Tesla',
        configurable: true,
        writable: true,
        enumerable: true
    },
    model: {
        value: 'S'
    },
    year: {
        value: 2012
    }
})


//More lightweight because it removes 'Proto' and the methods and properties of an obj
let carCreate2 = Object.create(null, {
    make: {
        value: ' Tesla',
    },
    model: {
        value: 'S'
    },
    year: {
        value: 2012
    }
})


console.log(car);
console.log(carCreate);
console.log(carCreate2);

for (const prop in car) {
    console.log(`Object Property: ${prop}: Property Value: ${car}`)
}

function Car(make, model, color, year) {
    // this.make = make;
    // this.model = model;
    // this.color = color;
    // this.year = year;
    Object.defineProperties(this, {
        make: {
            value: make,
        }   ,
        model: {
            value: model
        },
        color: {
            value: color
        },
        year: {
            value: year
        }
    })
};


const Tesla = new Car('Tesla', 'S', 'Red', 2012)

// console.log(Tesla);
// Tesla.make = 'ford';
// console.log(Tesla.make);

//call apply bind

let cpu = {
    card: '3080 TI',
    cores: 8,
    ram: 32,
    storage: '2 TB'
};

function BindinginAction () {
    console.log('read cpu');
    console.log(this.card);
    console.log(this.cores);
    console.log(this.ram);
    console.log(this.storage);
};

let binded = BindinginAction.bind(cpu);
binded();

let phone = {
    version: 12,
    make: 'Apple',
    newFeatures: 'none'
};

let newReleaseGreeting =  function(presenter, slidekick) {
    return `
    Meet ${presenter}. He will be going over this slide deck: ${slidekick} to go over our new phone:
    ${this.make} - ${this.version} - ${this.newFeatures}
    `
};

console.log(newReleaseGreeting.call(phone, 'Xavier Johnson', 'Super Time',))
console.log(newReleaseGreeting.apply(phone, ['Xavier Johnson', 'Super Time']));