const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const session = require ('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const flash = require('connect-flash');

const MONGODB_URI = 'mongodb://localhost:27017/twitter';
const PORT_NUMBER = 3000;

const home = require('./routes/home');
const dashboard = require('./routes/dashboard');
// const dashboard = require('./routes/dashboard');
const app = express();

const store = new MongoDBStore({ uri:MONGODB_URI, collection: 'sessions' });

app.use(
    session({
        store,
        secret: 'my secret'
    })
)

app.use(flash());
app.use(async (req, res, next) => {
    if (!req.session.user) {
        return next();
    }
})

app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(home);
// app.use('/', dashboard);

mongoose.connect(MONGODB_URI).then(() => {
    app.listen(PORT_NUMBER, () => {
        console.log(`Application running on port ${PORT_NUMBER}`);
    })
}).catch(err => console.log(err))