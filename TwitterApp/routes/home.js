const express = require('express');
const router = express.Router();

const { getHomePage, createAccount  } = require('../controllers/home');
// const { getDashboard } = require('../controllers/dashboard');

router.get('/', getHomePage);
router.post('/users', createAccount);

module.exports = router;
