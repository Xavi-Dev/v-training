const path = require('path');
const rootDirectory = require('../utils/path');


exports.getDashboard = (req, res) => {
    res.sendFile(path.join(rootDirectory, 'public', 'views', 'dashboard.html'));
}