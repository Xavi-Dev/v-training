const path = require('path');
const rootDirectory = require('../utils/path');

exports.getHomePage = (req, res) => {
    res.sendFile(path.join(rootDirectory, 'public', 'views', 'index.html'));
}

exports.dashboard = (req, res) => {
    res.sendFile(path.join(rootDirectory, 'public', 'views', 'index.html'));
}

exports.createUser = async (req, res) => {
    res.send({
        status:200,
        message: 'User made!'
    });
}

// app.controller('twitter', function($scope, $http) {
//     $scope.greeting = 'Hello World';
//     $scope.CreateUserEndpoint = 'http://localhost:3000/users';

//     $scope.createUser = function() {
//         $http.post($scope.CreateUserEndpoint, {
//             sample: 'testing'
//         });

//     }

// })