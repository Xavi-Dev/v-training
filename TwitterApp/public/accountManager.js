app.controller('accountManager', function($scope) {
    $scope.greeting = 'Hello Sensei, this is your login page';
    $http.get('').then(data => {
        $scope.accounts = data.data;
    })
    $scope.account = {};
    $scope.first = '';
    $scope.last = '';
    $scope.email = '';
    $scope.age = 0;

    $scope.createAccount = (updateId, account) => {
        if (updateId && account) {
            const {first, last, email } = account;
            $scope.first = first;
            $scope.last = last;
            $scope.email = email;
        } else {
            const id = $scope.uuidv4();
            const {first, last, email, age} = $scope;
            const createdAt = new Date();
            const body =JSON.stringify({
                first,
                last,
                email,
                age,
                createdAt
            });
            $http.patch(``, body)
                .then(() => {
                    delete $scope.accounts[id];
                });
                $scope.resetForm();
            }
        
    }

    $scope.uuidv4 = () => {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
         });
    }

    $scope.resetForm = function() {
        $scope.first = '';
        $scope.last = '';
        $scope.email = '';
        $scope.age = 0;
    }

    $scope.deleteAccount = function() {
        $http.delete(``)
        .then(() => {
            $scope.accounts[id] =JSON.parse(body);
        });
    }


});