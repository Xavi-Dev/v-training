let app = angular.module('twitter', ['ngRoute']);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'home'
        })
        .when('/dashboard', {
            templateUrl: 'views/dashboard.html',
            controller: 'dashboard'
        })
        .otherwise({
            redirectTo: '/'
        })
    $locationProvider.html5Mode(true);
})

app.controller('twitter', function ($scope, $http) {
    $scope.greeting = 'Hello World';
    $scope.CreateUserEndpoint = 'http://localhost:3000/users';

    $scope.createUser = function () {
        $http.post($scope.CreateUserEndpoint, {
            sample: 'testing'
        });
    }
})

