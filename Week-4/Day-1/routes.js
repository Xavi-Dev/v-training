const fs = require('fs');

const reqHandler = (req, res) => {
    const { url, method } = req;
    if (url === '/') {
        res.write(`
        <html>
            <head>
                <title>Page served from node</title>
            </head>
            <body>
                <form action="/name" method="POST">
                    <input type="text" name="name">
                    <button type="submit">Send</button>
                </form>
            </body>
        </html>`)
        return res.end();
    }
    if (url ==='/name' && method ==="POST") {
        // fs.writeFileSync('name.txt', "Xavier Johnson");
        
        //Process a req body
        const body = [];
        req.on('data', (chunk) => {
            body.push(chunk);
        })

        req.on('end', () => {
            const parsedBody = Buffer.concat(body).toString();
            // console.log(parsedBody);
            const [, name] = parsedBody.split('=');
            fs.writeFileSync('name.txt', name);
        });

        res.statusCode = 302;
        res.setHeader('Location', '/')
        return res.end();
    }
}

exports.handler = reqHandler;
exports.randomText = "My random text!"