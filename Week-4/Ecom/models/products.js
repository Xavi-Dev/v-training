const {Schema, model} = require('mongoose');

const product = new Schema({
    title: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        require: true
    },
    imageUrl: {
        type: String,
        required: true
    }
})

module.exports = model('Product', product);