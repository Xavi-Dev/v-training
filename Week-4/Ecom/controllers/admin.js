const Product = require('../models/products');
const path = require('path');
const rootDirectory = require('../utils/path');

exports.addProduct = async (req, res) => {
    const product = new Product({
        // title: req.body.title,
        // price: req.body.price,
        // description: req.body.description,
        // image: req.body.imageurl

        //or

        ...req.body

        //or 

        //req.body
    });
    await product.save();
    res.redirect('/')

}

exports.getAddProduct = (req, res) => {
    res.sendFile(path.join(rootDirectory, 'views', 'admin','product-form.html'));
}