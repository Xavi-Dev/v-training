const path = require('path');
const rootDirectory = require('../utils/path');
const Product = require('../models/products');

exports.getIndex = async (req, res) => {
    const prods = await Product.find();
    res.render('shop/index', {
        pageTitle: 'Shop',
        prods
    });
    console.log('Checking for products');
    console.log(prods);
}

exports.getProducts = async (req, res) => {
    const products = await Product.find();
    console.log('Checking for products');
    console.log(products);
}
