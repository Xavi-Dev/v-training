const path = require('path');
const express = require('express');
const shop = require('./routes/shop');
const mongoose = require('mongoose');
const admin = require('./routes/admin');

const app = express();

const MONGODB_URI = 'mongodb://localhost:27017/ecom'

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded());
app.use('/admin', admin);
app.use(shop);

app.set('view engine', 'ejs');
app.set('views', 'views');

mongoose.connect(MONGODB_URI). then(() => {
    app.listen(3000, () => console.log('app started on port: 3000 and connected to mongo!'));
}).catch(err => console.log(err));
