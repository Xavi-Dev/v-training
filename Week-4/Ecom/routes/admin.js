const express = require('express');

const {addProduct, getAddProduct} = require('../controllers/admin');

const router = express.Router();

router.get('/add-product', getAddProduct);
router.post('/add-product', addProduct);

module.exports = router;
