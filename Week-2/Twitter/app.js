(function() {


const signupEmailControl = document.getElementById('signupEmail');
const signupPassControl = document.getElementById('signupPass');
const signupButton = document.getElementById('signup');
const accounts = "https://twitter-clone-3452e-default-rtdb.firebaseio.com/people/.json"
signupButton.addEventListener('click', createAccount);

const loginEmailControl = document.getElementById('loginEmail');
const loginPassControl = document.getElementById('loginPass');
const loginButton = document.getElementById('loginbtn');
loginButton.addEventListener('click', login);



async function createAccount() {
    const email = signupEmailControl.value;
    const pass = signupPassControl.value;
    
    const body = JSON.stringify({
        email,
        pass
    });

    const newId = createUUID();
    console.log('The function works');
    console.log(body);

    await fetch(`https://twitter-clone-3452e-default-rtdb.firebaseio.com/people/${newId}/.json`, {
        method: 'PUT',
        body
    });

    clearInput();
}

function createUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
       var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
       return v.toString(16);
    });
 }


 async function login() {
    const emailLog = loginEmailControl.value;
    const passLog = loginPassControl.value;
    
    const data = await fetch(accounts);
    const results = await data.json();

    Object.keys(results).forEach(id => {
        const {
            email,
            pass
        } = results[id]; 
        if (email === emailLog && pass === passLog) {
            console.log('Account Exist');
            window.location.replace("./pages/dashboard.html")
        } else {
            alert('The email and password you entered did not match our records. Please double-check and try again.');
        }
    })
 }

    async function clearInput() {
        signupEmailControl.value = '';
        signupPassControl.value = '';
    }
//  function updateAccount() {

//  }


})();


