//Var - Let - Const
//Hoisting


//Example 1
console.log(a);
var a = 'hoistiing in action';

//Results undefined: Moves variable (Not Declaration) to top of scope.
//Declaration occurs after call = undefined.

//Example 2
console.log(b);
let b = 'Xavier';

//Result: let variables do not get hoisted / 'Cannot access variable before intialization'

//Example 3
console.log(c);
const c = 'Johnson'

//Results: same as a let, cannot be reassigned. 

const d = {
    a: 100,
    b: 200,
    c: 300
};

d.a = 500; //No error, only manipulating data; not reassining.

//Anything that is not a function does not create a scope

//Example 1
if (age === 100) {
    var varInIf = 'in if statement!';
    let letInIf = 'also in if statement';
}

console.log(varInIf); //this leaks out of if. globally scoped
console.log(letInIf); //only accessible inside code block -- throws error/Same with const;


//---------------------------------------------------------------------------------------

function sampleFunc() {
    console.log(x)
    //hoisting still occurs within functions, throws error undefined.
    console.log('sampleFunc executed!');
    var x = 'within sample func';
    console.log(x);
}

sampleFunc();
//var x unaccessible outside of function

//Everything in JS is globally scoped by default
function getSensitiveData() {
    return 'social security number is: xxxxxxxxxx';
}
//Result: can be called within browser console -- bad


//IIFE - Immediately Invoked Function Expression

(function() {
    function getSensitiveData() {
        return 'social security number is: xxxxxxxxxx';
    }
})();

//Result: function cannot be accessed from within anonymous function


(function(){
    var name = 'Joshua';
    var lastName = 'Johnson'
    (function() {
        console.log(name); //throws undefined cause var gets hoisted
        var name = 'Marwan';
        console.log(name); //throws Marwan
        console.log(lastName); //throws Johnson from outter function
    })();

    console.log(name);//throws Joshua
})

//--------------------------------------------------------------------------------------------

//Ways to declare functions

declaration();
expression();

function declaration() {
    console.log('Function Declaration');
}

var expression = function() {
    console.log('Function Expression');
}

//Difference = Function declarations get hoisted, expressions do not.

//mul function

let obj = {
    a: 100,
    b: 200,
    c: function() {
        console.log('within c!');
        return function() {
            console.log('within c but inside of c..');
            return function() {
                console.log('omg mul!');
            }
        }
    }
};

obj.c(); //calls only first function;
obj.c()(); //calls both functions.
console.log(obj.c()()); //throws function inside console


//------------------------------------------------------------------------
//Arrow Functions

const arrowTest = {
    a: 100,
    b: 200,
    c: function() {
        console.log(this) //returns a, b, and c
        return this.a + this.b; //returns 300
    }
}

const arrowTest2 = {
    a: 100,
    b: 200,
    c: function() {
        return function() {
            return this.a + this.b; //returns NaN because this is not binded to the objects anymore
        }
    }
}

const arrowTest3 = {
    a: 100,
    b: 200,
    c: function() {
        let _this = this;
        return function() {
            return _this.a + _this.b; //returns 300
        }
    }
}

const trueArrow = {
    a: 100,
    b: 200,
    c: function () {
        return () => {
            return this.a + this.b; //returns 300 cause arrow functions do not creat scope
        }
    },
    d: console.log(this),  //attaches to the window obj.
    e: function() {
        return () => this.a + this.b;
        }
    }
console.log (arrowTest.c());
console.log(arrowTest2.c()());
console.log(arrowTest3.c()());
console.log(trueArrow.c()());


//Value vs Reference Types

let a = 100;
let b = a;
a = 200;

console.log(b); //eqauls 100;

let c = {
    d: 200,
    e: 300
};

let f = c; //Reference type
c.d = 'what the heck??';

console.log(f.d); //Equals 'what the heck'


let original = { a: 100, b: 300 };

console.log(JSON.parse(original)); //converts a string into an object
console.log(JSON.stringify(original)); //turns Object into string;

//-------------------------------------------------------------------------------------

setTimeout(function() {
    console.log('testing ..');
}, 1000) //Waits 1 second to log in console.

for (var i = 0; i < 10; i++) {
    (function(j) {
        setTimeout(function() {
        console.log(i);
    }, 2000); 
})(i);
} //Prints 0 through 9. With no IIFE it prints 10 ten times.
//An alternative method is to swap var for let.


//-----------------------------------------------------------------------------------

//Object destructuring

let vehicle = {
    make: 'Audi',
    model: 'E-tron',
    color: 'Black and Red',
    interior: {
        iColor: 'black',
        airbags: 3
    }
}

// let make = vehicle.make;
// console.log(make);

let { make,
      model,
      color,
      interior:
       { iColor,
         airbags}
    } = vehicle;
console.log(make); //Same as dot notation.

//Array Destructuring

let names = ['Xavier', 'Princess', 'Mai Mai'];

let [xavier, princess] = names;
console.log(xavier);
console.log(princess);

//Spread operator

let house = {
    doors: 4, 
    height:'300 ft', 
}

let house2 = {
    ...house,
    color: 'red',
    doors: 8 //this will override the previous property due to its placement after the spread operator
}

function calculate(...nums){
    console.log(one); //logs only 1 value
    console.log(two); //logs only 2 value
    console.log(nums); //places value in array
    console.log(...nums); //places values in order
}

console.log(1, 2, 3, 4, 5, 6)

//shorthand property assignment

let cpu = 'AMD Ryzen';
let gpu = 'gtx 1660';
let ram = '16 gb';

let pc = {
    cpu,
    gpu,
    ram
}

console.log(pc);

//Instance functions

function Desk(width, height, color) {
    this.width = width;
    this.height = height;
    this.color = color;
}

let senseiDesk = new Desk ('15ft', '10ft', 'black');
console.log(senseiDesk);

//better way
//Prototypes define the relationship between functions and their constructors
Desk.prototype.fullDeskInfo = function () {
    const { 
        width,
        height,
        color
    } = this;
    return width + height + color;
}

