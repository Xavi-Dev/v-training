(function () {

    const getEndpoint = 'https://list-crud-72f74-default-rtdb.firebaseio.com/people/.json';
    const peopleTableBody = document.getElementById('people');
    const firstNameControl = document.getElementById('firstName');
    const lastNameControl = document.getElementById('lastName');
    const saveButton = document.getElementById('save');

    saveButton.addEventListener('click', createPerson)
    async function createPerson() {
        const first = firstNameControl.value;
        const last = lastNameControl.value;
        const body = JSON.stringify({first, last});

        const newId = createUUID();
        await fetch(`https://list-crud-72f74-default-rtdb.firebaseio.com/people//${newId}/.json`, {
            method: 'PUT',
            body
        });
        console.log(body);

        peopleTableBody.innerHTML += createTableRow(newId, first, last);
        clearFormControls();
    }

    async function deletePerson(id) {
        await fetch(`https://list-crud-72f74-default-rtdb.firebaseio.com/people//${id}/.json`,
        {
            method: 'DELETE'
        });

    }
 
    async function getPeople(endpoint) {
        const data = await fetch(endpoint);
        const result = await data.json();

        let tableRowString = '';
        // console.log(result);

        console.log(Object.keys(result));
        Object.keys(result).forEach(id => {
           const {
               first,
               last
           } = result[id];
           tableRowString += createTableRow(id, first, last);
        })
        // result.forEach((person, index) => {
        //     const { first, last } = person;
        //     tableRowString += createTableRow(index, first, last);
        // })

        peopleTableBody.innerHTML = tableRowString;
        delegateEvents();
    }

    function createUUID() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
           var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
           return v.toString(16);
        });
     }

    function clearFormControls() {
        firstNameControl.value = '';
        lastNameControl.value = '';
    }

    function createTableRow(id, first, last) {
        return `
        <tr>
            <th scope="row">${id}</th>
            <td>${first}</td>
            <td>${last}</td>
            <td>
                <button type="button" class="btn btn-primary">Update</button>
                <button data-personid="${id}" type="button" class="btn btn-danger">Delete</button>
            </td>
        </tr>
        `;
    }


    function delegateEvents() {
        peopleTableBody.addEventListener('click', (e) => {
            const { target: { 
                    nodeName, 
                    innerText, 
                    dataset: {
                        personid
            } } } = e;
            console.dir(e.target);
            if (nodeName === 'BUTTON' && innerText === 'Delete') {
                deletePerson(personid);
            } else if (nodeName === 'BUTTON' && innerText === 'Update') {

            }
        })
    }

    getPeople(getEndpoint);

})();