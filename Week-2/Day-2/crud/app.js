//Asynchronous request ^
// (function () {
//     const getButton = document.getElementById('getDrink');
//     const result = document.getElementById('result');
//     getButton.addEventListener('click', getRandomDrink);
    
//     function getRandomDrink () {
//         const request = new XMLHttpRequest();
//         request.onreadystatechange = () => {
//             if (request.readyState !==4) {
//                 console.log(request.readState);
//                 return;
//             }
//             if (request.status >= 200 && request.status < 300) {
//                 console.log(JSON.parse(request.responseText));
//                 const { strDrink } = JSON.parse(request.responseText).drinks[0];
//                 console.log(strDrink);
//                 result.innerText = strDrink;
//             }
//         };
//         request.open('GET', 'https://www.thecocktaildb.com/api/json/v1/1/random.php');
//         request.send();
//     };
// })();

//Callback function
// (function () {
//     const getButton = document.getElementById('getDrink');
//     const result = document.getElementById('result');
//     getButton.addEventListener('click',() => {
//         getRandomDrink((data) => {
//             result.innerText = data;
//         })
//     });
        
//     function getRandomDrink (resolve) {
//         const request = new XMLHttpRequest();
//         request.onreadystatechange = () => {
//             if (request.readyState !==4) {
//                 console.log(request.readyState);
//                 return;
//             }
//             if (request.status >= 200 && request.status < 300) {
//                 console.log(JSON.parse(request.responseText));
//                 const { strDrink } = JSON.parse(request.responseText).drinks[0];
//                 // console.log(strDrink);
//                 resolve(strDrink);
//             }
//         };
//         request.open('GET', 'https://www.thecocktaildb.com/api/json/v1/1/random.php');
//         request.send();
//     };
// })();


//Promise Function
(function () {
    const getButton = document.getElementById('getDrink');
    const result = document.getElementById('result');
    const drinkEndpoint = 'https://www.thecocktaildb.com/api/json/v1/1/random.php'
    getButton.addEventListener('click',() => {
        // getRandomDrink((data) => {
        //     result.innerText = data;
        // })
        getRandomDrinkWithPromise().then(data => {
            console.log(data);
        }).catch(error => {
            console.log(error);
        });
//Promise within Promise
        // fetchDrink(drinkEndpoint).then( data => {
        //     data.json().then(realData => {
        //         console.log(realData);
        //     });
        // })

        // fetchDrink(drinkEndpoint).then(data => data.json()).then(results => console.log(results));
    });
        
    function getRandomDrinkWithPromise () {
        const request = new XMLHttpRequest();
        return new Promise((resolve, reject) => {
            request.onreadystatechange = () => {
                if (request.readyState !== 4) {
                    console.log(request.readyState);
                    return;
                }
                if (request.status >= 200 && request.status < 300) {
                    console.log(JSON.parse(request.responseText));
                    const { strDrink } = JSON.parse(request.responseText).drinks[0];
                    // console.log(strDrink);
                    resolve(strDrink);
                } else {
                    const {status, statusText } = request;
                    reject({
                        status,
                        statusText
                    })
                }
            };
            request.open('GET', 'https://www.thecocktaildb.com/api/json/v1/1/random.php');
            request.send();
        })

    };

    //Fetch Function
    // function fetchDrink(url) {
    //     return fetch(url);
    // }
})();
