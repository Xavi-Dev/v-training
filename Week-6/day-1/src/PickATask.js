import React from 'react';
import Header from './Header';
import Options from './Options';
import AddOption from './AddOption';

export default class PickATask extends React.Component {
    constructor() {
        super();

        this.state = {
            options: []
        }
    }

    handleDeleteOption = (option) => {
        this.setState(currentState => ({ options: currentState.options.filter(optionEl => option !== optionEl) }))
    }

    deleteAll = () => {
        this.setState({ options: []})
    }

    render() {
        return(
            <div>
                <Header title="Pick A Task!" subTitle="This app will help you pick a task to start"/>
                <div>
                    <div>
                        <Options handleDeleteOptions={this.handleDeleteOption} deleteAll={this.deleteAll} options={this.state.options} />
                    </div>
                </div>
            </div>
        )
    }
}

