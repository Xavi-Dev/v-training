import React from 'react';

export default class AddOption extends React.Component {
    render() {
        return (
            <div>
                <form>
                    <input type="text" name="option" />
                    <button className="button">Add Option</button>
                </form>
            </div>
        )
    }
}