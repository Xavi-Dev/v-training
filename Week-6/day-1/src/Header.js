import React from 'react';


const Header = ({title, subTitle}) => (
    <div>
    <h1>{title}</h1>
    <h2>{subTitle}</h2>
</div>
)

export default Header;