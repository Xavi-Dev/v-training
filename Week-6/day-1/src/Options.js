import React from 'react';
import Option from './Option';



const Options = ({ options, deleteAll, handleDeleteOption }) => {
    <div>
        <div>
            <h3>Your Options</h3>
            { !!options.length !== 0 && <button onClick={deleteAll}>Remove All</button> }
            { !options.length && <p>Please add an option to get started!</p> }
            {
                options.map((options, index) => <Option key={index} handleDeleteOption={handleDeleteOption} optionText={option}/>) 
            }
        </div>
    </div>
}

export default Options;