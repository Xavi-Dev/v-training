import React from 'react';


const Option =({ optionText, handleDeleteOption }) => {
    <div>
        <p>{ optionText }</p>
        <button onClick={ () => {handleDeleteOption(optionText)} }>Remove</button>
    </div>
};

export default Option;