import React from 'react';
import ReactDOM from 'react-dom';
import PickATask from './PickATask';


ReactDOM.render(<PickATask />, document.getElementById('app'));


// class Counter extends React.Component {
//     constructor() {
//         super();
//         this.state = {
//             count: 0
//         }

//         this.addOne = this.addOne.bind(this)
//         this.minusOne = this.minusOne.bind(this)
//         this.reset = this.reset.bind(this)

//     }

//     addOne() {
//         // this.setState((currentState)) => {
//         //     return {
//         //         count: currentState.count + 1
//         //     }
//         // }
//         this.setState(currentState => ({ count: currentState.count + 1}))
//     }

//     minusOne(){
//         this.setState(currentState => ({ count:currentState.count -1}))
//     }

//     reset(){ 
//         this.setState({count: 0})
//     }

//     render() {
//         return (
//             <div>
//                 <h1>Count: {this.state.count} </h1>
//                 <button onClick={this.addOne}>+1</button>
//                 <button onClick={this.reset}>reset</button>
//                 <button onClick={this.minusOne}>-1</button>
//             </div>
//         )
//     }
// };